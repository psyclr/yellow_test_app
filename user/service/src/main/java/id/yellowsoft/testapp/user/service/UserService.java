package id.yellowsoft.testapp.user.service;

import id.yellowsoft.testapp.user.dto.UserDto;

import java.util.List;

public interface UserService
{
	List<UserDto> getUsers();
}
