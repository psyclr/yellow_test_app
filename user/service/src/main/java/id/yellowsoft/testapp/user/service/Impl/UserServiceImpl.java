package id.yellowsoft.testapp.user.service.Impl;

import id.yellowsoft.testapp.user.dto.UserDto;
import id.yellowsoft.testapp.user.entity.User;
import id.yellowsoft.testapp.user.repository.UserRepository;
import id.yellowsoft.testapp.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("userService")
public class UserServiceImpl implements UserService
{
	private final UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository)
	{
		this.userRepository = userRepository;
	}

	@Override
	public List<UserDto> getUsers()
	{
		List<User> all = userRepository.findAll();
		return all.stream().map(UserDto::new).collect(Collectors.toList());
	}
}
