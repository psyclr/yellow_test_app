package id.yellowsoft.testapp.user.controller;

import id.yellowsoft.testapp.user.dto.UserDto;
import id.yellowsoft.testapp.user.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/auth")
public class ResourceController {

	@Resource(name = "userService")
	private UserService userService;

	@RequestMapping(value ="/users", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public List<UserDto> getUsers(){
		return userService.getUsers();
	}
}