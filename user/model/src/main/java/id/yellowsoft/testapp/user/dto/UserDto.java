package id.yellowsoft.testapp.user.dto;

import id.yellowsoft.testapp.user.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserDto implements UserDetails
{
	private static final String USER_TYPE = "USER";
	private Long identity;
	private String username;
	private String password;
	private Collection<GrantedAuthority> authorities;
	private String image;

	public UserDto(User user)
	{
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.authorities = AuthorityUtils.createAuthorityList(USER_TYPE);
		this.image = user.getImage();
	}
	public UserDto()
	{
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return authorities;
	}

	@Override
	public String getPassword()
	{
		return password;
	}

	@Override
	public String getUsername()
	{
		return username;
	}

	@Override
	public boolean isAccountNonExpired()
	{
		return true;
	}

	@Override
	public boolean isAccountNonLocked()
	{
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	@Override
	public boolean isEnabled()
	{
		return true;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}
	public Long getIdentity()
	{
		return identity;
	}

	public void setIdentity(Long identity)
	{
		this.identity = identity;
	}

}