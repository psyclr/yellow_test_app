package id.yellowsoft.testapp.user.repository;

import id.yellowsoft.testapp.user.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long>
{
	User findByUsername(String username);

	@Override
	List<User> findAll();
}
