//package id.yellowsoft.testapp.application.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//@EnableWebSecurity
//public class ApplicationWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter
//{
//    @Value("${security.signing-key}")
//    private String signingKey;
//
//    @Value("${security.encoding-strength}")
//    private Integer encodingStrength;
//
//    @Value("${security.security-realm}")
//    private String securityRealm;
//
//
//	@Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("user1").password("user1Pass").roles("USER");
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/login*").anonymous()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .defaultSuccessUrl("/homepage")
//                .failureUrl("/login.html")
//                .and()
//                .logout().logoutSuccessUrl("/login");
//    }
//}
